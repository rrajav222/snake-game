from turtle import Turtle
import random

possible_food_coordinates = []
for i in range(-360, 360, 20):
    possible_food_coordinates.append(i)


class Food(Turtle):
    def __init__(self, segments):
        super().__init__()
        self.shape("circle")
        self.speed("fastest")
        self.color("yellow")
        self.shapesize(stretch_wid=0.8, stretch_len=0.8)
        self.refresh(segments)

    def refresh(self, segments):
        random_x = random.choice(possible_food_coordinates)
        random_y = random.choice(possible_food_coordinates)
        for each_segment in segments:
            segment_x = round(each_segment.xcor())
            segment_y = round(each_segment.ycor())
            #print(f"segment at {segment_x}, {segment_y}")
            #print(f"food at {random_x}, {random_y}")

            if random_x == segment_x and random_y == segment_y:
                #print("food position changed")
                self.refresh(segments)

        self.up()
        self.goto((random_x, random_y))
