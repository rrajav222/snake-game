from scoreboard import Scoreboard
from turtle import Screen
from snake import Snake
from food import Food
import time


def snake_game():
    screen = Screen()
    screen.clear()
    screen.setup(width=800, height=800)
    screen.bgcolor('black')
    screen.title('Python ;)')
    screen.tracer(0)

    score = Scoreboard()
    snake = Snake()
    food = Food(snake.segments)

    screen.listen()
    game_is_on = True
    while game_is_on:
        screen.update()
        time.sleep(0.07)
        snake.move()
        screen.onkey(snake.turnLeft, "Left")
        screen.onkey(snake.turnRight, "Right")
        screen.onkey(snake.turnUp, "Up")
        screen.onkey(snake.turnDown, "Down")

        if snake.head.distance(food) < 1:
            #print("eaten")
            snake.extend()
            food.refresh(snake.segments)
            score.increaseScore()

        if snake.head.xcor() > 380 or snake.head.xcor() < -380 or snake.head.ycor() > 380 or snake.head.ycor() < -380:
            game_is_on = False
            # print(snake.head.xcor(),snake.head.ycor())
            score.gameOver()

        for each_segment in snake.segments[1:]:
            if snake.head.distance(each_segment) < 10:
                game_is_on = False
                score.gameOver()
                # print(snake.head.position(),each_segment.position())


    screen.onkey(snake_game, "space")

    screen.exitonclick()


snake_game()