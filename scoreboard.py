from turtle import Turtle



ALIGNMENT = "center"
FONT = ("Times New Roman", 20, "normal")

class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        with open("data.txt") as data_file:
            data_high_score= data_file.read()
        self.score = 0
        self.high_score = int(data_high_score)
        self.color('tomato')
        self.hideturtle()
        self.up()
        self.goto(0, 350)
        self.updateScore()

    def updateScore(self):
        self.clear()
        if self.score > self.high_score:
            self.high_score = self.score
            with open("data.txt", mode="w") as data_file:
                data_file.write(str(self.high_score))
        self.write(f"Score = {self.score}   High Score = {self.high_score}", move=False,
                   align=ALIGNMENT, font=FONT)

    def increaseScore(self):
        self.score += 1
        self.updateScore()

    def gameOver(self):
        self.goto(0, 0)
        self.write(f"GAME OVER!!! Score = {self.score}", move=False,
                   align=ALIGNMENT, font=FONT)
        self.goto(0, -30)
        self.write("Press [SPACE] key to play again", move=False,
                   align=ALIGNMENT, font=FONT)
