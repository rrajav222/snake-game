from turtle import Turtle


STARTING_POSITION = [(0, 0), (-20, 0), (-40, 0)]
MOVE_DISTANCE = 20
LEFT = 0
UP = 90
RIGHT = 180
DOWN = 270


class Snake(Turtle):
    def __init__(self):
        super().__init__()
        self.segments = []
        self.create_snake()
        self.head = self.segments[0]
        self.head.color("deepskyblue")

    def create_snake(self):
        for position in STARTING_POSITION:
            self.add_segment(position)

    def add_segment(self, position):
        segment = Turtle()
        segment.color('white')
        segment.shape('square')
        segment.up()
        segment.goto(position)
        self.segments.append(segment)

    def extend(self):
        last_position = self.segments[-1].position()
        self.add_segment(last_position)

    def move(self):
        for seg_num in range(len(self.segments)-1, 0, -1):
            if seg_num != 0:
                self.segments[seg_num].goto(
                    self.segments[seg_num-1].position())
        self.head.fd(MOVE_DISTANCE)

    def turnLeft(self):
        if self.head.heading() != LEFT:
            self.head.setheading(RIGHT)

    def turnRight(self):
        if self.head.heading() != RIGHT:
            self.head.setheading(LEFT)

    def turnUp(self):
        if self.head.heading() != DOWN:
            self.head.setheading(UP)

    def turnDown(self):
        if self.segments[0].heading() != UP:
            self.segments[0].setheading(DOWN)


# class Segment:
#    def __init__(self, starting_position):
#        self.segment = Turtle()
#        self.segment.speed(1)
#        self.segment.color('white')
#        self.segment.shape('square')
#        self.segment.up()
#        self.segment.goto(starting_position)
#
#
# class Snake:
#    def __init__(self):
#        self.snake = [
#            Segment(starting_position=STARTING_POSITION[0]),
#            Segment(starting_position=STARTING_POSITION[1]),
#            Segment(starting_position=STARTING_POSITION[2])
#        ]
#
#    def move(self):
#        for seg_num in range(len(self.snake)-1, 0, -1):
#            if seg_num != 0:
#                self.snake[seg_num].segment.goto(
#                    self.snake[seg_num-1].segment.position())
#        self.snake[0].segment.fd(20)
